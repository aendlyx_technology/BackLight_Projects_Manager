from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from pymongo import MongoClient






class Database(QWidget):
	UpdateSignal = pyqtSignal()
	def __init__(self, parent):
		super(QWidget, self).__init__(parent)
		self.initDatabase()
		self.initDictionaries()
		self.initAdmin()
		self.Projects()
		self.UpdateSignal.connect(self.Update)






	def getProjects(self):
		self.Projects()
		return self._projects





	def getCategories(self):
		return self._categories





	def getRessources(self):
		return self._ressources





	def getFilteredRessources(self,filter):
		filtered = {}
		for name,ressource in self._ressources.items():
			if name.find(filter) != -1:
				filtered[name] = ressource
		return filtered





	def getStatus(self):
		return self._admin['Status']





	def Projects(self):
		self._projects.clear()
		Pnames = self._B4CKL1GHT.collection_names(False)
		for name in Pnames:
			if name != "admin" and name != "users":
				self._projects[name] = self._B4CKL1GHT[name]





	def nextBKLID(self,project):
		tmp = 0
		for bklid in self._projects.keys():
			tmp += 1	
			if tmp == len(self._projects):
				sid = bklid.split('_')
				id = int(sid[0])
				#print(id)
				id += 1
				#print(id)
				if len(str(id)) == 1:
					return "00" + str(id) + '_' + project
				elif len(str(id)) == 2:
					return "0" + str(id) + '_' + project
				elif len(str(id)) == 3:
					return str(id) + '_' + project





	def Update(self):
		project  = self._z['Explorer'].getSelectedProject()
		self._categories.clear()
		self._ressources.clear()
		for category in self._projects[project].find({"Type" : "Category"}):
			cname = category['Name']
			self._categories[cname] = category
			for ressource in self._projects[project].find({"Type" : "Ressource", "Category" : cname}):
				rname = ressource['Name']
				rname += '_' + cname
				self._ressources[rname] = ressource
				del rname
			del cname
		del project
		for key, value in self._ressources.items():
			print(f"key [{key}] value [{value}]")
		# print(self._categories)





	def saveRessource(self, name, listwidget):
		project = self._z['Explorer'].getSelectedProject()
		category = self._z['Explorer'].getSelectedCategory()
		request = {}
		request['Category'] = category
		request['Name'] = name
		for doc in self.findInCollection(project, request):
			for key,value in doc.items():
				if key == 'Tasks':
					for cnt, data in enumerate(value):
						data['Status'] = listwidget[cnt + 3][1].text()
						data['User'] = listwidget[cnt + 3][2].text()
		self._projects[project].save(doc)
		self.Update()
		print(f"saved ressource {doc}")
		del request
		del doc





	def addAdminModel(self, request):
		self._4DM1N.insert_one(request)





	def addUser(self, request):
		self._US3RS.insert_one(request)





	def getUsers(self):
		return self._US3RS.find({})	





	def getAdminModel(self, modelname):
		return self._admin[modelname]





	def getAdmin(self):
		return self._admin





	def getCategoryTask(self):
		l = []
		category = self._z['Explorer'].getSelectedCategory()
		if category != "Projects":
			for model in self._admin['Category']:
				if model['Name'] == category:
					if 'Tasks' in model:
						for task in model['Tasks']:
							l.append(task)
					else:
						return None
			return l





	def getMaxTasks(self):
		res = 0
		for model in self._admin['Category']:
			if 'Tasks' in model.keys():
				cnt = 0
				for task in model['Tasks']:
					cnt += 1
				if cnt > res:
					res = cnt
		return res





	def checkUser(self, login, password):
		for user in self._US3RS.find({"Name" : login, "Password" : password}):
			return user





	def existInCollection(self,collection,request):
		request.pop('Tasks', None)
		res = self._projects[collection].find(request).count()
		if res == 0:
			return False
		elif res > 0:
			return True




	def insertInCollection(self, collection, request):
		self._projects[collection].insert_one(request)





	def findInCollection(self, collection, request):
		return self._projects[collection].find(request)





	def deleteInCollection(self, collection, request):
		return self._projects[collection].delete_many(request)





	def createCollection(self, project):
		self._B4CKL1GHT[project].insert_one({"Type" : "Project"})
		self.Projects()





	def dropCollection(self, project):
		self._projects[project].drop()





	def initDatabase(self):
		self._MongoClient = MongoClient('mongodb://192.168.116.26:27017/')
		self._B4CKL1GHT = self._MongoClient['B4CKL1GHT']
		self._4DM1N = self._B4CKL1GHT.admin
		self._US3RS = self._B4CKL1GHT.users





	def initAdmin(self):
		for mtype in self._4DM1N.find({}):
			mtypename = mtype['Type']
			if mtypename not in self._admin.keys():
				self._admin[mtypename] = []
				for mname in self._4DM1N.find({"Type" : mtypename}):
					self._admin[mtypename].append(mname)
		del mtypename





	def initDictionaries(self):
		self._admin = {}
		self._projects = {}
		self._categories = {}
		self._ressources = {}
