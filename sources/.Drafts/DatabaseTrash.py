		# Projects { PName : { CName : [{Ressource1},{Ressource2}] } }
		# Projects[Test][Assets] = [{Ressource1},{Ressource2}]
		# Projects[Test] = {"Assets" : [{}{}]} 
		self._Projects = {}
		self._Connexions = {}
		Pnames = self._B4CKL1GHT.collection_names(False)
		for name in Pnames:
			self._Projects[name] = {None : None}
			self._Connexions[name] = self._B4CKL1GHT[name]
		del Pnames
		categories = []
		for name, con in self._Connexions.items():
			categories.clear()
			for category in con.find({"Type" : "Category"}):
				if category['Name'] not in categories:
					categories.append(category['Name'])
			for category in categories:
				self._Projects[name][category] = []
				for ressource in con.find({"Type" : "Ressource", "Category" : category}):
					self._Projects[name][category].append(ressource)
		print(self._Projects


	# Categories()
	# Update categories & categoriesid {}
	# -------------------------------------------------------------------------------
	# categories 		{ Name 		  		:   ObjectId 		 		 }
	# categproesid		{ ObjectId 		 	: [ Field1, Field2, Field3 ] }
	# -------------------------------------------------------------------------------
	# Request the project selected in the explorer	
	# Ask the Database for Categories Objects
	# Store the Object_Id
	# Attribute the Object_Id to categories {} with the name of the category as the Key
	# Create a List in categoriesid {} with the Object_Id as the Key
	# Ask the Database for the Specified Object_Id
	# Iterate the fields on the Object Retrieved
	# Append each field to the List

	def Categories(self):
		print("Database.Categories()")
		project = self.z['Explorer']._Project
		for category in self.findInCollection(project, {"Type" : "Category"})
			object_id = category['_id']
			self._categories[category['Name']] = object_id
			self._categoriesid[object_id] = []
			for fields in self.findInCollection(project, {"_id" : object_id}):
				for field in fields:
					self._categoriesid[object_id].append(field)
		print(".....................")

	# Ressources()
	# Update ressource & ressourcesid {}
	# -------------------------------------------------------------------------------
	# ressources 		{ "Category" : [ ObjectId0, ObjectId1, ...] }
	# ressourcesid		{ ObjectId 	 : [ ressource['Name'], ressource['Type'], ...] }
	# -------------------------------------------------------------------------------
	# Get Project Selected
	# Get Category Selected
	# Append a list into the ressource[Category] dict
	# Get ObjectId corresponding to the category
	# Ask DB which ressources belong to this category
	# store the ressource Objectid in the ressource[category] list
	# iterate the stored Objectid(s)
	# ask db for the ObjectId
	# store all the ObjectId fields in ressourcesid[objectid] list

	def Ressources(self):
		print("Database.Ressources")
		project = self.z['Explorer']._Project
		category = self.z['Explorer']._Category
		self._ressources[category] = []
		categoryid = self._categories[category]
		for ressource in self.findInCollection(project, {"Type" : "Ressource","Category" : categoryid}):
			self._ressources[category].append(ressource['_id'])
		for ressource in self._ressources[category]:
			objectid = ressource
			self._ressourcesid[objectid] = []
			for fields in self.findInCollection(project, {"_id" : objectid}):
				for field in fields:
				self._ressourcesid[objectid].append(field)
		print("...................")

	# Tasks()
	# Update tasks & tasksid {}
	# --------------------------------------------------------------------------
	# tasks 			{ "Ressources" : ObjectId }
	# tasksid			{  ObjectId 	: [ task['Status'], task['User'], ...] }
	# --------------------------------------------------------------------------

	def Tasks(self):
		print("Database.Tasks()")

		print("................")

# V 1 

	

	def getCategories(self):
		print("Database.getCategories()")
		self._categories.clear()
		# 000_None : self._BKL_PROJECTS['000_None']
		project = self._Parent._Explorer._Project
		for a in self.findInCollection(project, {"Type" : "Category"}):
			if a['Name'] in self._categories:
				print("categories already exist")
			else:
				self._categories.append(a['Name'])
		print(self._categories)
		print(".....................................")

	def getCatRessources(self):
		print("Database.getCatRessources()")
		self._ressources.clear()
		tmp = None
		project = self._Parent._Explorer._Project
		for a in self._categories:
			category = {"Type" : "Ressource", "Category" : a}
			self._ressources[a] = []
			for b in self.findInCollection(project, category):
				tmp = b['Name']
				self._ressources[a].append(tmp)
		del project
		del category
		del tmp
		print(".............................................")

	def filterRessources(self, category, filt):
		print("Database.filterRessources()")
		self._filtered.clear()
		for a in self._categories[category]:
			if a in filt:
				self._filtered.append(a)
		print("...........................")

	def getRessourceTasks(self):
		print("Database.getRessourceTasks()")
		self._tasks.clear()
		project = self._Parent._Explorer._Project
		task = {}
		for category, ressources in self._ressources.items():
			task['Type'] = "Task"
			task['Category'] = category
			for a in ressources:
				task['Ressource'] = a
				self._tasks[a] = []	
				for b in self.findInCollection(project, task):
					self._tasks[a].append(b)
		
		print(".............................................")

	def insertInCollection(self, collection, request):
		print("Database.insertInCollection()")
		self._projects[collection].insert_one(request)
		print(".....................................")

	def deleteInCollection(self, collection, request):
		print("Database.deleteInCollection()")
		return self._projects[collection].delete_many(request)
		print(".............................................")

	def findInCollection(self, collection, request):
		print("Database.findInCollection()")
		return self._projects[collection].find(request)
		print("......................................")

	def createCollection(self, collection):
		print("Database.createCollection()")
		tmp = collection
		collection = self.BKLID() + "_" + tmp
		project = {"Project" : collection, "Role" : "Master"}
		self._BKL_PROJECTS[collection].insert_one(project)
		self.getProjects()
		del project
		print("............................................")

	def dropCollection(self, collection):
		print("Database.dropCollection()")
		self._projects[collection].drop()
		self._projects.pop(collection)
		print(".........................")

	def getUser(self, user, password):
		print("Database.getUser()")
		for w in self._users.find({"User" : user, "PWD" : password}):
			if w != None:
				return True
		return False
		print("....................................................")

	def BKLID(self):
		print("Database.BKLID()")
		print(self._projects)
		tmp = 0
		for bklid, connexion in self._projects.items():
			tmp += 1	
			if tmp == len(self._projects):
				sid = bklid.split('_')
				id = int(sid[0])
				print(id)
				id += 1
				print(id)
				if len(str(id)) == 1:
					return "00" + str(id)
				elif len(str(id)) == 2:
					return "0" + str(id)
				elif len(str(id)) == 3:
					return str(id)
		print(".....................................")

for adminitem in adminlist:
			self._admin[adminitem['Type']] = {}
			for dbitem in self._AdminDB.find(adminitem):
				for key, value in dbitem.items():
					self._admin[adminitem['Type']][dbitem['Name']][key] = value
					# self._admin[dbitem['Name']][key] = value
		for test, test2 in self._admin.items():					
			print(test)
			print(test2)

		adminlist.append(categorymodel)
		adminlist.append(taskmodel)
		adminlist.append(categorytemplate)
		adminlist.append(rank)
		adminlist.append(taskstatus)


		# self._categorymodel = []
		# self._admin[categorymodel['Type']] = {}
		# for dbitem in self._AdminDB.find(categorymodel):
		# 	self._categorymodel.append(dbitem)
		# 	self._admin[categorymodel['Type']][dbitem['Name']] = dbitem
		# self._taskmodel = []
		# for dbitem in self._AdminDB.find(taskmodel):
		# 	self._taskmodel.append(dbitem)
		# self._categorytemplate = []
		# for dbitem in self._AdminDB.find(categorytemplate):
		# 	self._categorytemplate.append(dbitem)
		# self._rank = []
		# for dbitem in self._AdminDB.find(rank):
		# 	self._rank.append(dbitem)
		# self._taskstatus = []
		# for dbitem in self._AdminDB.find(taskstatus):
		# 	self._taskstatus.append(dbitem)
		# print(self._categorymodel)

				# print(self._taskmodel)
		# print(self._categorytemplate)
		# print(self._rank)
		# print(self._taskstatus)

