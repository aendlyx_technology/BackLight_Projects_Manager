from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class User(QWidget):
	MWResized = pyqtSignal()
	def __init__(self, parent):
		super(QWidget,self).__init__(parent)
		#print("User.__init__()")
		self.initZone(parent)
		self._mainwindow 	= None
		self.MWResized.connect(self.resize)
		#
		#
		#
		#
		#
	def getAuthorization(self, action):
		if action == "AddProject":
			if self._Rank.text() == "MasterAdmin":
				#print("Authorized to add a Project")
				return True
		#
		#
		#
		#
		#
	def EditorUserError(self, message):
			self._z['Messages'].setText(message)
			self.EditorUserClose()
		#
	def EditorUserClose(self):
		self._eduw[1].clear()
		self._EditorDialogUser.hide()
		self._EditorDialog.hide()
		#
	def EditorUserCreate(self):
		database = self._z['Database']
		request = {}
		request['Type'] = "User"
		request["Name"] = self._eduw[1].text()
		request["Rank"] = self._eduw[2].currentText()
		database.addUser(request)
		self.EditorUserClose()
		del database
		del request
		#
	def EditorUser(self):
		if self._EditorDialogUser == None:
			self._EditorDialogUser = QDialog(self._z['ProjectsManager'])
			self._EditorDialogUser.setWindowTitle("User Editor")
			self._eduw = [QLabel("Name", self._EditorDialogUser),
						  QLineEdit(self._EditorDialogUser),
						  QComboBox(self._EditorDialogUser),
						  QPushButton("Create User", self._EditorDialogUser)]
			admin = self._z['Database'].getAdminModel("Rank")
			for r in admin:
				self._eduw[2].addItem(r['Name'])
			self._eduw[3].clicked.connect(self.EditorUserCreate)
			self._eduwb = QVBoxLayout(self._EditorDialogUser)
			for w in self._eduw:
				self._eduwb.addWidget(w)
			del admin
		self._EditorDialogUser.show()			
		#
		#
		#
		#
		#
	def EditorStatusError(self, message):
			self._z['Messages'].setText(message)
			self.EditorStatusClose()
		#
	def EditorStatusClose(self):
		self._edsw[1].clear()
		self._edsw[3].clear()
		self._edsw[5].clear()
		self._edsw[7].clear()
		self._EditorDialogStatus.hide()
		self._EditorDialog.hide()
		#
	def EditorStatusCreate(self):
		database = self._z['Database']
		request = {}
		request['Type'] = "Status"
		request["Name"] = self._edsw[1].text()
		request["bg_color"] = self._edsw[3].text()
		request["text_color"] = self._edsw[5].text()
		request["Index"] = self._edsw[7].text()
		database.addAdminModel(request)
		self.EditorStatusClose()
		del database
		del request
		#
	def EditorStatus(self):
		if self._EditorDialogStatus == None:
			self._EditorDialogStatus = QDialog(self._z['ProjectsManager'])
			self._EditorDialogStatus.setWindowTitle("Status Editor")
			self._edsw = [QLabel("Name", self._EditorDialogStatus),
						  QLineEdit(self._EditorDialogStatus),
						  QLabel("bg_color", self._EditorDialogStatus),
						  QLineEdit(self._EditorDialogStatus),
						  QLabel("text_color", self._EditorDialogStatus),
						  QLineEdit(self._EditorDialogStatus),
						  QLabel("Index", self._EditorDialogStatus),
						  QLineEdit(self._EditorDialogStatus),
						  QPushButton("Create Rank",self._EditorDialogStatus)]
			self._edsw[8].clicked.connect(self.EditorStatusCreate)
			self._edswb = QVBoxLayout(self._EditorDialogStatus)
			for w in self._edsw:
				self._edswb.addWidget(w)
		self._EditorDialogStatus.show()		
		#
		#
		#
		#
		#
	def EditorRankError(self, message):
			self._z['Messages'].setText(message)
			self.EditorRankClose()
		#
	def EditorRankClose(self):
		self._edrw[1].clear()
		self._EditorDialogRank.hide()
		self._EditorDialog.hide()
		#
	def EditorRankCreate(self):
		database = self._z['Database']
		request = {}
		request['Type'] = "Rank"
		request["Name"] = self._edrw[1].text()
		database.addAdminModel(request)
		self.EditorRankClose()
		del database
		del request
		#
	def EditorRank(self):
		if self._EditorDialogRank == None:
			self._EditorDialogRank = QDialog(self._z['ProjectsManager'])
			self._EditorDialogRank.setWindowTitle("Rank Editor")
			self._edrw = [QLabel("Name", self._EditorDialogRank),
						  QLineEdit(self._EditorDialogRank),
						  QPushButton("Create Rank",self._EditorDialogRank)]
			self._edrw[2].clicked.connect(self.EditorRankCreate)
			self._edrwb = QVBoxLayout(self._EditorDialogRank)
			for w in self._edrw:
				self._edrwb.addWidget(w)
		self._EditorDialogRank.show()		
		#
		#
		#
		#
		#
	def EditorTaskError(self, message):
			self._z['Messages'].setText(message)
			self.EditorTaskClose()
		#
	def EditorTaskClose(self):
		self._edtw[1].clear()
		self._edtw[3].clear()
		self._EditorDialogTask.hide()
		self._EditorDialog.hide()
		#
	def EditorTaskCreate(self):
		database = self._z['Database']
		request = {}
		request['Type'] = "Task"
		request["Name"] = self._edtw[1].text()
		request["Extension"] = self._edtw[3].text()
		database.addAdminModel(request)
		self.EditorTaskClose()
		del database
		del request
		#
	def EditorTask(self):
		if self._EditorDialogTask == None:
			self._EditorDialogTask = QDialog(self._z['ProjectsManager'])
			self._EditorDialogTask.setWindowTitle("Task Editor")
			self._edtw = [QLabel("Name", self._EditorDialogTask),
						  QLineEdit(self._EditorDialogTask),
						  QLabel("Extension", self._EditorDialogTask),
						  QLineEdit(self._EditorDialogTask),
						  QPushButton("Create Task",self._EditorDialogTask)]
			self._edtw[4].clicked.connect(self.EditorTaskCreate)
			self._edtwb = QVBoxLayout(self._EditorDialogTask)
			for w in self._edtw:
				self._edtwb.addWidget(w)
		self._EditorDialogTask.show()		
		#
		#
		#
		#
		#
	def EditorCategoryError(self, message):
			self._z['Messages'].setText(message)
			self.EditorCategoryClose()
		#
	def EditorCategoryClose(self):
		self._edcw[1].clear()
		self._edcw[3].clear()
		self._edcw[5].clear()
		self._EditorDialogCategory.hide()
		self._EditorDialog.hide()
		#
	def EditorCategoryCreate(self):
		database = self._z['Database']
		request = {}
		request['Type'] = "Category"
		request["Name"] = self._edcw[1].text()
		request["Index"] = self._edcw[3].text()
		request["Color"] = self._edcw[5].text()
		request["Tasks"] = []
		for a in range(7, (len(self._edcw) - 1)):
			if self._edcw[a].currentText() != "None":
				request["Tasks"].append(self._edcw[a].currentText())
		database.addAdminModel(request)
		self.EditorCategoryClose()
		del database
		del request
		#
	def EditorCategory(self):
		if self._EditorDialogCategory == None:
			self._EditorDialogCategory = QDialog(self._z['ProjectsManager'])
			self._EditorDialogCategory.setWindowTitle("Category Editor")
			self._edcw = [QLabel("Name", self._EditorDialogCategory),
						  QLineEdit(self._EditorDialogCategory),
						  QLabel("Index", self._EditorDialogCategory),
						  QLineEdit(self._EditorDialogCategory),
						  QLabel("Color", self._EditorDialogCategory),
						  QLineEdit(self._EditorDialogCategory),
						  QLabel("Select tasks to associate with the category or None if not", self._EditorDialogCategory)]
			admin = self._z['Database'].getAdminModel("Task")
			for r in admin:
				self._edcw.append(QComboBox(self._EditorDialogCategory))
			for a in range(7, (len(self._edcw) - 1)):
				self._edcw[a].addItem("None")
			for r in admin:
				for a in range(7, (len(self._edcw) - 1)):
					self._edcw[a].addItem(r['Name'])
			self._edcw.append(QPushButton("Create Category",self._EditorDialogCategory))
			self._edcw[len(self._edcw) - 1].clicked.connect(self.EditorCategoryCreate)
			self._edcwb = QVBoxLayout(self._EditorDialogCategory)
			for w in self._edcw:
				self._edcwb.addWidget(w)
			del admin
		self._EditorDialogCategory.show()
		#
		#
		#
		#
		#
	def Editor(self):
		#print("User.Editor()")
		if self._EditorDialog == None:
			self.buildEditor()
		else:
			self._EditorDialog.show()
		#
	def buildEditor(self):
		self._EditorDialog = QDialog(self._z['ProjectsManager'])
		self._EditorDialog.setWindowTitle("Which Editor do you need ?")
		# Buttons Model Type
		self._modeltypebtns = []
		database = self._z['Database']
		for modeltype in database.getAdmin().keys():
			self._modeltypebtns.append(QPushButton(modeltype, self._EditorDialog))
		self._modeltypebtns.append(QPushButton("User", self._EditorDialog))
		self._hboxmodeltypebtns = QHBoxLayout(self._EditorDialog)
		for btn in self._modeltypebtns:
			self._hboxmodeltypebtns.addWidget(btn)
		self._modeltypebtns[0].clicked.connect(self.EditorCategory)
		self._modeltypebtns[1].clicked.connect(self.EditorTask)
		self._modeltypebtns[2].clicked.connect(self.EditorRank)
		self._modeltypebtns[3].clicked.connect(self.EditorStatus)
		self._modeltypebtns[4].clicked.connect(self.EditorUser)
		self._EditorDialogCategory = None
		self._EditorDialogTask = None
		self._EditorDialogRank = None
		self._EditorDialogStatus = None
		self._EditorDialogUser = None
		self._EditorDialog.show()
		#
		#
		#
		#
		#
	def displayUser(self):
		self._Login.hide()
		self._Password.hide()
		self._User.show()
		self._User.setText(self._user)
		self._Rank.show()
		self._Rank.setText(self._rank)
		self._Editor.show()
		self._Disconnect.show()
		#
	def connectUser(self):
		user = self._Login.text()
		password = self._Password.text()
		database = self._z['Database']
		result = database.checkUser(user, password)
		if result != None:
			self._user = result['Name']
			self._rank = result['Rank']
			self.displayUser()
		else:
			self._z['Messages'].setText("Verify Login and Password")
		del user
		del password
		del database
		del result
		#
	def disconnectUser(self):
		self._Login.clear()
		self._Password.clear()
		self._Login.show()
		self._Password.show()
		self._User.hide()
		self._Rank.hide()
		self._Editor.hide()
		self._Disconnect.hide()
		# self._z['Messages'].clear()
		#
		#
		#
		#
		#
	def initZone(self,parent):
		self._Zone			= QWidget(parent)
		self._Zone.setStyleSheet("QWidget {background-color : #E4E5DC;}")
		self._Profile 		= QLabel("Profile", self._Zone)
		self._Login 		= QLineEdit(self._Zone)
		self._Password 		= QLineEdit(self._Zone)
		self._Password.editingFinished.connect(self.connectUser)
		self._User			= QPushButton(self._Zone)
		self._Rank			= QPushButton(self._Zone)
		self._Editor		= QPushButton("Editor", self._Zone)
		self._Editor.clicked.connect(self.Editor)
		self._Disconnect	= QPushButton("Disconnect",self._Zone)
		self._Disconnect.clicked.connect(self.disconnectUser)
		self._User.hide()
		self._Rank.hide()
		self._Editor.hide()
		self._Disconnect.hide()
		self._EditorDialog = None
		#
		#
		#
		#
		#
	def resize(self):
		W = self._mainwindow['Width']
		H = self._mainwindow['Height']
		self._Zone.setGeometry(W * 0.005,H * 0.01 ,W * 0.15, H * 0.15)
		self._Profile.setGeometry(0, 0, W * 0.075, H * 0.15)
		self._Login.setGeometry(W * 0.075, H * 0.05, W * 0.075, H * 0.025)
		self._Password.setGeometry(W * 0.075, H * 0.075, W * 0.075, H * 0.025)
		self._User.setGeometry(W * 0.075, H * 0, W * 0.075, H * 0.0375)
		self._Rank.setGeometry(W * 0.075, H * 0.0375, W * 0.075, H * 0.0375)
		self._Editor.setGeometry(W * 0.075, H * 0.075, W * 0.075, H * 0.0375)
		self._Disconnect.setGeometry(W * 0.075, H * 0.1125, W * 0.075, H * 0.0375)