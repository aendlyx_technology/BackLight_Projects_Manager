
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import os





class Grid(QWidget):
	"""
	the grid is the central elements to browse ressource and assign task to user, and modify their status
	it is composed of a vertical CQListWidget called Grid
	and many horizontals CQListWidget(s) called Lines
	"""
	MWResized = pyqtSignal()
	def __init__(self, parent):
		super(QWidget,self).__init__(parent)
		self.initZone(parent)


# # # U P D A T E # # #


	def Update(self):
		"""
		update the content of the grid, change headers and ressources displayed depending on the selected category in the explorer
		"""
		if self._Grid.count() == 1:
			self.Status()
			self.Users()
		self.Headers()
		self.Lines()


# # # H E A D E R S # # # 


	def Headers(self):
		"""
		the headers are a list of qpushbutton placed at the top of the grid 
		"""
		self.getCategoryTasks()
		self.setHeaders()
		




	def getCategoryTasks(self):
		"""
		get the headers name corresponding to the category plus name and preview who are always there
		"""
		self._Tasks.clear()
		self._Tasks = self._z['Database'].getCategoryTask()
		l = ["Name", "Preview"]
		for task in self._Tasks:
			l.append(task)
		self._Tasks = l





	def setHeaders(self):
		"""
		depending on the number of headers set their name and hide the useless or display the missing
		"""
		lentasks = len(self._Tasks)
		self._Grid.setRowHidden(0, False)
		if self._Tasks != None:
			if lentasks > self._Lines[0][0].count():													# more Tasks than Headers ?
				for cnt in range(self._Lines[0][0].count(), lentasks):									# len(Headers) to len(Tasks)
					self._Lines[0].append(self.Header(self._Tasks[cnt], self._Lines[0][0]))				# widget Header with task name
			else:																						# more Headers than Tasks ?
				for cnt in range(1, (lentasks + 1)):													# 1 to len(l) + 1 cuz 0 is qlistw
					self._Lines[0][cnt][1].setText(self._Tasks[cnt - 1])								# set new Names
					self._Lines[0][0].setRowHidden(cnt, False)											# display the missing header setted
				for cnt in range(lentasks, self._Lines[0][0].count()):									# len(l) to len(Headers)
					self._Lines[0][0].setRowHidden(cnt, True)											# hide the useless Headers
		self._Grid.setItemWidget(self._Grid.item(0), self._Lines[0][0])									# set the qlistw into the Grid[0]
		self._Grid.item(0).setSizeHint(QSize(100 ,28))													# fix size of the headers list 





	def Header(self, name, qlistw):
		"""
		custom widget header everything is a list starting by a parent qwidget because it is simple like that to assign at a CQListWidget
		"""
		header = [QWidget(qlistw)]
		header.append(QPushButton(name, header[0]))
		header[1].setGeometry(0,0,95,24)
		qlistw.addItem("")
		qlistw.setItemWidget(qlistw.item(qlistw.count() - 1), header[0])
		qlistw.item(qlistw.count() - 1).setSizeHint(QSize(95,24))
		return header


# # # L I N E S # # #


	def Lines(self):
		"""
		the lines are constructed and displayed depending on the ressources collected
		the ressources are corresponding to the selected category in the explorer
		then they are filtered or not and ordered by alphabetical order
		"""
		self.getCategoryRessources()
		
		lenressources = len(self._Ressources)
		gridcount = self._Grid.count()														
		if lenressources > ( gridcount - 1 ):										  						# more Ressources than Lines
			self.createLine(lenressources,gridcount)
		else:
			self.updateLine(lenressources,gridcount)
			


	def createLine(self, lenressources, gridcount):
			for cnt in range(gridcount, (lenressources + 1)):												# start 1 cuz 0 = Headers to len Ressources + 1 cuz of Headers
				self._Grid.addItem("")																		# 1 : prepare the empty line and fix it size 
				self._Grid.item(cnt).setSizeHint(QSize(1000	,100))
				self._Lines.append([CQListWidget(self._Grid)])												# 2 : instanciate a CQListWidget corresponding to the line set it as horizontal list
				self._Lines[cnt][0].setFlow(CQListWidget.LeftToRight)										
				self._Lines[cnt][0].Grid = self
				self._Lines[cnt].append(self.Name("", self._Lines[cnt][0]))									# 3 : append Name custom widget to the line list and assign it to the qlist
				self._Lines[cnt].append(self.Preview(self._Lines[cnt][0]))									# 4 : append Preview custom widget to the line list and assign it to the qlist
				self._Grid.setItemWidget(self._Grid.item(self._Grid.count() - 1),self._Lines[cnt][0])		# 5 : setting line to the grid and fix it size
				self._Grid.item(gridcount - 1).setSizeHint(QSize(100,100))									# 6 : build maximum amount of tasks ready to be displayed
				self.Tasks(cnt)																				# 7 : create default tasks





	def updateLine(self, lenressources, gridcount):
		for cnt, name in enumerate(self._Ressources.keys()):
			self.updateName(cnt + 1, name)
			self.updatePreview(cnt + 1, name)
			self.updateTasks(cnt + 1, self._Ressources[name])
			self.hide(self._Grid, cnt + 1)
		for cnt in range(lenressources + 1, gridcount + 1):														
			self.show(self._Grid, cnt)





	def clickedLine(self):
		sender = self.sender()
		self._z['Tools'].updatePath("RessourceGrid", sender.text())
		# print(sender)

	def hide(self, qlistw, cnt):
		qlistw.setRowHidden(cnt, False)




	def show(self, qlistw, cnt):
		qlistw.setRowHidden(cnt, True)





	def getCategoryRessources(self):
		category = self._z['Explorer'].getSelectedCategory()
		self._Ressources.clear()
		if self._z['Tools'].getFilter() != None:
			for fname, fressource in self._z['Database'].getFilteredRessources(self._z['Tools'].getFilter()).items():
				if fressource['Category'] == category:
					split = fname.split('_')
					self._Ressources[split[0]] = fressource
		else:
			for name, ressource in self._z['Database'].getRessources().items():
				if ressource['Category'] == category:
					split = name.split('_')
					self._Ressources[split[0]] = ressource
					del split
		order = {}
		orderl = []
		orderl = sorted(self._Ressources.keys(), key=lambda x:x.lower())
		for name in orderl:
			order[name] = self._Ressources[name]
		del orderl
		self._Ressources.clear()
		for key, value in order.items():
			self._Ressources[key] = value
		del order

# # # N A M E # # #


	def Name(self, name, qlistw):
		qlistw.addItem("")
		Name = []
		Name.append(QWidget(qlistw))
		Name.append(QPushButton(name,Name[0]))
		Name[1].setGeometry(0,0,95,95)
		Name[1].clicked.connect(self.clickedLine)
		qlistw.setItemWidget(qlistw.item(qlistw.count() - 1),Name[0])
		qlistw.item(qlistw.count() - 1).setSizeHint(QSize(95,95))
		return Name





	def updateName(self, line, name):
		self._Lines[line][1][1].setText(name)


# # # P R E V I E W # # # 


	def Preview(self, qlistw):
		qlistw.addItem("")
		Preview = []
		Preview.append(QPushButton(qlistw))
		Preview.append(QIcon(""))
		Preview[0].setGeometry(0,0,95,95)
		qlistw.setItemWidget(qlistw.item(qlistw.count() - 1), Preview[0])
		qlistw.item(qlistw.count() - 1).setSizeHint(QSize(95,95))
		return Preview





	def updatePreview(self, line, path):
		Path = "./data/" 
		Path += self._z['Explorer'].getSelectedProject() + '/'
		Path += self._z['Explorer'].getSelectedCategory() + '/'
		Path += path + "/nopreview.png"
		Preview = self._Lines[line][2]
		Preview[1].addFile(Path)
		Preview[0].setIcon(Preview[1])
		Preview[0].setIconSize(QSize(83,83))


# # # T A S K S # # #


	def Tasks(self, index):
		maxtasks = self._z['Database'].getMaxTasks()
		if maxtasks > (self._Lines[index][0].count() - 2):
			for cnt in range((self._Lines[index][0].count() - 2), maxtasks):
				self._Lines[index].append(self.Task(self._Lines[index][0]))





	def Task(self, qlistw):
		qlistw.addItem("")
		Task = []
		Task.append(QWidget(qlistw))
		Task.append(HoverButton("None",Task[0]))
		Task[1].Grid = self
		#1
		Task[1].setContextMenuPolicy(Qt.CustomContextMenu)
		Task[1].customContextMenuRequested.connect(self.taskMenu)
		#
		Task.append(HoverButton("", Task[0]))
		Task[2].Grid = self
		Task[2].setContextMenuPolicy(Qt.CustomContextMenu)
		Task[2].customContextMenuRequested.connect(self.userMenu)
		Task[1].setGeometry(0,0,95,60)
		Task[2].setGeometry(0,60,95,35)
		qlistw.setItemWidget(qlistw.item(qlistw.count() - 1),Task[0])
		qlistw.item(qlistw.count() - 1).setSizeHint(QSize(95,95))
		return Task





	def updateTasks(self, index, ressource):
		start = 3
		end = (len(self._Tasks) + 1)
		for cnt in range(start, end):
			# print(f"line[{cnt}][{self._Lines[index][cnt]}")
			self._Lines[index][0].setRowHidden(cnt, False)
			self._Lines[index][cnt][1].setText(ressource['Tasks'][cnt - 3]['Status'])
			for status in self._Status.values():
				if status['Name'] == self._Lines[index][cnt][1].text():
					self._Lines[index][cnt][1].setStyleSheet("QPushButton {background-color: " + status['bg_color'] + "; color:" + status['text_color'] + ";}")
			self._Lines[index][cnt][2].setText(ressource['Tasks'][cnt - 3]['User'])
		start = (len(self._Tasks))
		end = self._Lines[index][0].count()
		for cnt in range(start, end):
			self._Lines[index][0].setRowHidden(cnt, True)
		del start
		del end





	def updateTask(self, sender, mode):
		if mode == 2:
			for w in self._selectedwc:
				w.setText(sender.text())
				self.saveLineTasks(w,0)
			self._selectedwc.clear()
			self._shift = False
			self.Update()
		elif mode == 3:
			for w in self._selectedwc:
				w.setText(sender.text())
				self.saveLineTasks(w,1)
			self._selectedwc.clear()
			self._shift = False
		else:
			self._rsender.setText(sender.text())
			self.saveLineTasks(self._rsender,mode)





	def saveLineTasks(self, sender, mode):
		for index, line in enumerate(self._Lines):
			if index != 0:
				for cnt, widget in enumerate(line):
					if cnt > 2:
						if mode == 0:
							if sender == widget[1]:
								name = self._Lines[index][1][1].text()
								self._z['Database'].saveRessource(name,self._Lines[index])
						elif mode == 1:
							if sender == widget[2]:
								name = self._Lines[index][1][1].text()
								self._z['Database'].saveRessource(name,self._Lines[index])
		self.Update()





	def selectMultiTasks(self, sender):
		if self._shift == True:
			for index, line in enumerate(self._Lines):
				if index != 0:
					lentasks = len(self._Tasks) + 1
					for cnt in range(3,lentasks):
						print(line)
						print(f"{line[cnt]} ===== {cnt}")
						if line[cnt][1] == sender:
							if self._selecting == 0:
								# print(line[cnt])
								self._selectedw.append(sender)
								self._selecting += 1
							elif self._selecting == 1:
								# print(line[cnt])
								self._selectedw.append(sender)
								self._selecting += 1
								self.highlightSelected()
						# elif line[cnt][2] == sender:
						# 	if self._selecting == 0:
						# 		self._selectedw.append(sender)
						# 		self._selecting += 1
						# 	elif self._selecting == 1:
						# 		self._selectedw.append(sender)
						# 		self._selecting += 1
						# 		self.highlightSelected()	
				if self._selecting == 2:
					self._selecting = 0
					self._selectedw.clear()


	def highlightSelected(self):
		print(self._selectedw)
		start = self._selectedw[0]
		end = self._selectedw[1]
		adding = False
		self._selectedw.clear()
		for line in self._Lines:
			lentasks = len(self._Tasks) + 1
			for cnt in range(3, lentasks):
				if line[cnt][1] == start:
					adding = True
				elif line[cnt][1] == end:
					print(f"adding line[{cnt}][1]")
					self._selectedw.append(line[cnt][1])
					adding = False
				if adding == True:
					print(f"adding line[{cnt}][1]")
					self._selectedw.append(line[cnt][1])
		for w in self._selectedw:
			w.setStyleSheet("QPushButton {background-color : green}")
		self._selectedwc = self._selectedw.copy()
		del adding
		del start
		del end
		del lentasks


	def TaskSelected(self):
		print("yo")





	def taskMenu(self, point):
		self._rsender = None
		self._rsender = self.sender()
		self.popMenuTasks.exec_(self._rsender.mapToGlobal(point))





	def userMenu(self, point):
		self._rsender2 = None
		self._rsender = self.sender()
		self.popMenuUsers.exec_(self._rsender.mapToGlobal(point))





	def Status(self):
		self._Status = {}
		for status in self._z['Database'].getStatus(): # get the rank of the connected user
			self._Status[status['order']] = status
		self.popMenuTasks = QMenu("Tasks",self)
		for value in self._Status.values():
			act = QAction(value['Name'], self)
			pixmap = QPixmap(10,10)
			color = QColor()
			color.setNamedColor(value['bg_color'])
			pixmap.fill(color)
			icon = QIcon(pixmap)
			act.setIcon(icon)
			act.triggered.connect(self.actStatusTriggered)
			self.popMenuTasks.addAction(act)




 
	def actStatusTriggered(self):
		sender = self.sender()
		if len(self._selectedwc) > 0:
			self.updateTask(sender, 2)
		else:
			self.updateTask(sender,0)





	def Users(self):
		self._Users = {}
		for user in self._z['Database'].getUsers():
			if user['Rank'] not in self._Users.keys():
				self._Users[user['Rank']] = []
			self._Users[user['Rank']].append(user)
		self.popMenuUsers = QMenu("Users",self)
		for rank, user in self._Users.items():
			submenu = QMenu(rank, self)
			self.popMenuUsers.addMenu(submenu)
			for u in user:
				act = QAction(u['Name'], self)
				act.triggered.connect(self.actUserTriggered)
				submenu.addAction(act)





	def actUserTriggered(self):
		sender = self.sender()
		self.updateTask(sender,1)





	def hideGrid(self):
		for cnt, line in enumerate(self._Lines):
			self._Grid.setRowHidden(cnt, True)

# # # I N I T # # #


	def initZone(self,parent):
		self._Zone = QWidget(parent)
		self._Zone.setStyleSheet("QWidget {background-color : #E4E5DC;}")
		self._Grid = CQListWidget(self._Zone)
		self._Grid.Grid = self
		self._Grid.addItem("")
		self._Lines = [[CQListWidget(self._Grid)]]
		self._Lines[0][0].setFlow(CQListWidget.LeftToRight)
		self._Lines[0][0].Grid = self
		self._Ressources = {}
		self._Tasks = []
		self._UTMode = 0
		self._mainwindow = None
		self.MWResized.connect(self.resize)
		self._shift = False
		self._selecting = 0
		self._selectedw = []
		self._selectedwc = None






	def resize(self):
		W = self._mainwindow['Width']
		H = self._mainwindow['Height']
		self._Zone.setGeometry(W * 0.16, H * 0.17, W * 0.68, H * 0.75)
		self._Grid.setGeometry(0,0, W * 0.68, H * 0.75)





class HoverButton(QPushButton):
	mouseHover = pyqtSignal(bool)
	def __init__(self, text, parent=None):
		QPushButton.__init__(self, text, parent)
		self.setMouseTracking(True)
		self.setText(text)
		self.pressed.connect(self.Mpressed)
		self.clicked.connect(self.Mclicked)
		# self.released.connect(self.Mreleased)

	def enterEvent(self, event):
		self.mouseHover.emit(True)

	def leaveEvent(self, event):
		self.mouseHover.emit(False)

	def Mclicked(self):
		print("clicked")

	def Mpressed(self):
		self.Grid.selectMultiTasks(self)


class CQListWidget(QListWidget):
	def __init__(self, parent=None):
		QListWidget.__init__(self,parent)

	def mousePressEvent(self, QMouseEvent):
		print(QMouseEvent.pos())