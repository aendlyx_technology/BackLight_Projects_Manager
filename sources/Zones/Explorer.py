
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *





class Explorer(QWidget):
	MWResized = pyqtSignal()
	UpdateSig = pyqtSignal()
	def __init__(self, parent):
		super(QWidget,self).__init__(parent)
		self.initZone(parent)
		self.initResize()
		self.UpdateSig.connect(self.Update)





	def Update(self):
	  #.1 save the content of the explorer cbox ( if not the first use of Update() because on the first use nothing has to be saved )
		if self._mainwindow != None:
			self.saveExplorerState()
	  #.																												
		self._Categories.clear()																						
		for tab in self._CategoriesWidgets:				
			for cnt, w in enumerate(tab):
				if cnt < 5:
					w.deleteLater()																						
	  #.																												
		self._CategoriesWidgets.clear()																					
	  #.																												
		self._CatItems = 0																								
	  #.
		self.getSelectedProject()																						
	  #.
		self._z['Database'].UpdateSignal.emit()																			
	  #.
		self.addItem("Projects")																						
		for category in self._z['Database'].getCategories().keys():		
			self.addItem(category)																						
	  #.
		if self._mainwindow != None:
			self.restoreExplorerState()
	  #.
		if self._mainwindow != None:
			self.resize()





	def saveExplorerState(self):
		# Sauvegarder le nom du projet selectionne
		self._Project = self.getCboxText("Projects")
		# Sauvegarder toute les categories disponible dans le project
		if len(self._CategoriesWidgets) > 1:
			for category in self._CategoriesWidgets:
				self._savedCategory.append(self.getCboxText(category[5]))





	def restoreExplorerState(self):
		self.focusCbox("Projects", self._Project)
		if len(self._CategoriesWidgets) > 1:
			for cnt, category in enumerate(self._CategoriesWidgets):
				self.focusCbox(category[5], self._savedCategory)





	def listItemSelected(self):	
		for a in range(0,self._Categories.count()):
			if self._Categories.item(a).isSelected():
				for model in self._z['Database'].getAdminModel("Category"):
					category = self._CategoriesWidgets[a][5]
					if model['Name'] == category:
						self._CategoriesWidgets[a][0].setStyleSheet("QWidget {background-color : " + model['Color'] + ";}")
						self._CategoriesWidgets[a][2].setStyleSheet("QWidget {background-color : #E4E5DC;}")
						self._CategoriesWidgets[a][3].setStyleSheet("QWidget {color : #E4E5DC;}")
						self._CategoriesWidgets[a][4].setStyleSheet("QWidget {background-color : #E4E5DC;}")
						self._z['Tools'].setButtons(self._CategoriesWidgets[a][3].text())
						self._Category = self._CategoriesWidgets[a][5]
						if category != "Projects":
							self._z['Grid'].Update()
						else:
							self._z['Grid'].hideGrid()
						self._z['Database'].getCategoryTask()
						if a != 0:
							self._z['Tools'].updatePath("Category", None)
						else :
							self._z['Tools'].updatePath("None", None)
			else:		
				self._CategoriesWidgets[a][0].setStyleSheet("QPushButton {background-color : #E4E5DC;}")
				self._CategoriesWidgets[a][3].setStyleSheet("QWidget {color : black;}")
				for model in self._z['Database'].getAdminModel("Category"):
					category = self._CategoriesWidgets[a][3].text()
					if model['Name'] == category:
						self._CategoriesWidgets[a][2].setStyleSheet("QWidget {background-color : " + model['Color'] + ";}")





	def projectsActivated(self):
		#print("projectsActivated")
		self.Update()
		self.resize()
		self._z['Tools'].updatePath("Projects", None)
		# self._z['Grid'].Clear()





	def categoryActivated(self):
		self._z['Tools'].updatePath("Ressource", None)





	def addItem(self,category):

		# get model properties

		for model in self._z['Database'].getAdminModel("Category"):
			if model['Name'] == category:

				# add an empty item to the qlist
				self._Categories.addItem("")

				# build the list item widget composed of multiples widgets appended in an array
				ItemExplorer = []
				ItemExplorer.append(QWidget(self._z['ProjectsManager']))
				ItemExplorer = [ItemExplorer[0],
								QPushButton(ItemExplorer[0]),
								QPushButton(ItemExplorer[0]),
								QLabel(model['Name'],ItemExplorer[0]),
								QComboBox(ItemExplorer[0]),
								model['Name']]

				# when an item is selected in the cbox update the path in tools								
				# ItemExplorer[4].activated.connect(self._z['Tools'].updatePath) #
				
				# customize the buttons and background color depending on the model associated
				ItemExplorer[1].setStyleSheet("QPushButton {background-color:"+ model['Color'] +";}")
				ItemExplorer[2].setStyleSheet("QPushButton {background-color:"+ model['Color'] +";}")

				# when a project is changed reload the explorer
				if category == "Projects":
					ItemExplorer[4].activated.connect(self.projectsActivated)
				else:
					ItemExplorer[4].activated.connect(self.categoryActivated)
				# append the custom widget into the list of widgets corresponding to the qlis items 
				self._CategoriesWidgets.append(ItemExplorer)

				# set the custom widget into the qlist
				ListItem = self._Categories.item(self._CatItems)
				ItemWidget = self._CategoriesWidgets[self._CatItems][0]
				self._Categories.setItemWidget(ListItem, ItemWidget)

				# connect whats going on when you click on one item
				self._Categories.itemClicked.connect(self.listItemSelected)

				# # # # self._Categories.itemClicked.connect(self._z['Tools.updatePath'])

				# refresh the content of the cbox corresponding
				self.refreshCbox(category)

				# add 1 to the counter of items avalaibles in the list
				self._CatItems += 1





	def refreshCbox(self, category):
		database = self._z['Database']
		#print(f"refreshCbox({category})")
		if category == "Projects":
			projects = database.getProjects()
			# self.saveSelectedProject()
			self.clearCbox(category)
			self.cboxAddItems(category, projects)
			# self.restoreSelectedProject()
			del projects
		else:
			ressources = database.getRessources()
			# self.saveSelectedCategory()
			self.clearCbox(category)
			self.cboxAddItems(category, ressources)
			# self.restoreSelectedCategory()
			del ressources
		del database





	def cboxAddItems(self, category, items):
		cat = self.findCategory(category)
		if category == "Projects":
			for key in items.keys():
				cat[4].addItem(key)
		else:
			for key, value in items.items():
				if value['Category'] == category:
					split = key.split('_')
					cat[4].addItem(split[0])
					del split





	def focusCbox(self,category, name):
		for ccategory in self._CategoriesWidgets:
			if category == ccategory[5] :
				for cnt in range(0,ccategory[4].count()):
					if ccategory[4].itemText(cnt) == name:
						text = ccategory[4].setCurrentIndex(cnt)
						ccategory[4].setCurrentIndex(cnt)





	def clearCbox(self, category):
		cat = self.findCategory(category)
		cat[4].clear()





	def findCategory(self, category):
		for cat in self._CategoriesWidgets:
			if cat[5] == category:
				#print(cat[5])
				return cat





	def getCboxText(self,category):
		category = self.findCategory(category)
		return category[4].currentText()





	def getSelectedProject(self):
		return self._Project





	def getSelectedCategory(self):
		if self._CategoriesWidgets[0][4].currentText() != self._Project:
			return self._CategoriesWidgets[0][4].currentText()
		else:
			return self._Category






	def initZone(self,parent):
		self._Zone = QWidget(parent)
		self._Zone.setStyleSheet("QWidget {background-color : #E4E5DC;}")
		self._Categories = QListWidget(self._Zone)
		self._CategoriesWidgets = []
		self._CatItems = 0
		self._Project = "000_None"
		self._Category = None
		self._savedCategory = [] 





	def initResize(self):
		self._mainwindow 				= None
		self.MWResized.connect(self.resize)





	def resize(self):
		W = self._mainwindow['Width']
		H = self._mainwindow['Height']
		self._Zone.setGeometry(W * 0.005, H * 0.17, W * 0.15, H * 0.75)
		self._Categories.setGeometry(0,0, W * 0.15, H * 0.75)
		for a in range(0,self._CatItems):
			self._Categories.item(a).setSizeHint(QSize(W * 0.14,H * 0.05))
			self._CategoriesWidgets[a][0].setGeometry(0,0,W * 0.14,H * 0.05)
			self._CategoriesWidgets[a][1].setGeometry(0,0, W * 0.004,H * 0.05)
			self._CategoriesWidgets[a][2].setGeometry(W * 0.01,H * 0.0125, W * 0.017, H * 0.025)
			self._CategoriesWidgets[a][3].setGeometry(W * 0.035,0, W * 0.11,H * 0.020)
			self._CategoriesWidgets[a][4].setGeometry(W * 0.035,H * 0.0219, W * 0.108,H * 0.025)