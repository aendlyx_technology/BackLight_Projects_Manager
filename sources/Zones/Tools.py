
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
import os
import shutil
from time import gmtime, strftime





class Tools(QWidget):
	MWResized = pyqtSignal()
	def __init__(self, parent):
		super(QWidget,self).__init__(parent)
		self.initZone(parent)





	def initFilter(self):
		self._Filter = QLineEdit(self._Zone)
		self._FilterHistory = []
		self._Filter.returnPressed.connect(self.runFilter)





	def getFilter(self):
		if self._Filter.text() == "":
			return None
		else:
			return self._Filter.text()





	def historyFilter(self):
		if len(self._FilterHistory) > 10:
			self._FilterHistory.pop(0)
		self._FilterHistory.append(self._Filter.Text())





	def runFilter(self):
		self._z['Grid'].Update()





	def initScaleUI(self):
		self._ScaleSlide = QSlider(Qt.Horizontal, self._Zone)
		self._ScaleNum = QLineEdit(self._Zone)





	def initPath(self):
		self._Path = QLineEdit(self._Zone)
		self._OpenPath = QPushButton("Open Path in Explorer", self._Zone)





	def updatePath(self, sender, data):
		if sender == "Projects":
			text = self._z['Explorer'].getSelectedProject()
			self._Path.setText(text + '/')
			del text
		elif sender == "Category":
			text = self._z['Explorer'].getSelectedProject() + '/'
			text += self._z['Explorer'].getSelectedCategory() + '/'
			self._Path.setText(text)
			del text
		elif sender == "Ressource":
			text = self._z['Explorer'].getSelectedProject() + '/'
			text += self._z['Explorer'].getSelectedCategory() + '/'
			text += self._z['Explorer'].getCboxText(self._z['Explorer'].getSelectedCategory()) + '/'
			self._Path.setText(text)
			del text
		elif sender == "RessourceGrid":
			text = self._z['Explorer'].getSelectedProject() + '/'
			text += self._z['Explorer'].getSelectedCategory() + '/'
			text += data + '/'
			print(self.sender())
			self._Path.setText(text)
			del text
		elif sender == "None":
			self._Path.setText("")





	def initButtons(self):
		self._B1 = QPushButton(self._Zone)
		self._B1.clicked.connect(self.B1Clicked)
		self._B2 = QPushButton(self._Zone)
		self._B2.clicked.connect(self.B2Clicked)
		self._B3 = QPushButton(self._Zone)
		self._B3.clicked.connect(self.B3Clicked)
		self._B4 = QPushButton(self._Zone)
		self._B4.clicked.connect(self.B4Clicked)
		self._textbuttons = None





	def setButtons(self, category):
		if self._textbuttons == None:
			self._textbuttons = {"Project" : ["Add Project", "Del Project", "Add Category", "Del Category"],
								 "Category" : ["Add ", "Del ", "Add Task", "Del Task"]}
		if category == "Projects":
			self._B1.setText(self._textbuttons['Project'][0])
			self._B2.setText(self._textbuttons['Project'][1])
			self._B3.setText(self._textbuttons['Project'][2])
			self._B4.setText(self._textbuttons['Project'][3])
		else:
			B1 = self._textbuttons['Category'][0] + category
			self._B1.setText(B1)
			B2 = self._textbuttons['Category'][1] + category
			self._B2.setText(B2)
			self._B3.setText(self._textbuttons['Category'][2])
			self._B4.setText(self._textbuttons['Category'][3])
			del B1
			del B2





	def B1Clicked(self):
		if self._B1.text() == "Add Project":
			self.popupAddProject()
		else:
			self.popupAddRessource()





	def B2Clicked(self):
		if self._B2.text() == "Del Project":
			self.popupDelProject()
		else:
			self.popupDelRessource()





	def B3Clicked(self):
		if self._B3.text() == "Add Category":
		        self.popupAddCategory()
		else:
			self.popupAddTask()





	def B4Clicked(self):
		if self._B4.text() == "Del Category":
			self.popupDelCategory()
		else:
			self.popupDelTask()





	def initPopups(self):
		self.popAddProject = None
		self.popDelProject = None
		self.popAddCategory = None
		self.popDelCategory = None
		self.popAddRessource = None
		self.popDelRessource = None
		self.popAddTask = None
		self.popDelTask = None





	def AddProject(self):
		db = self._z['Database']
		explorer = self._z['Explorer']
		user = self._z['User']
		if user.getAuthorization("AddProject") == True:
			project = self.popAddProjectW[1].text()
			project = db.nextBKLID(project)
			db.createCollection(project)
			self.createPath("Projects", project)
			self._z['Explorer'].Update()
			explorer.focusCbox("Projects",project)
			explorer.UpdateSig.emit()
			del project
		self.popAddProjectW[1].clear()
		self.popAddProject.hide()
		del db
		del explorer
		del user





	def popupAddProject(self):
		if self.popAddProject == None:
				self.popAddProject  =  QDialog(self._z['ProjectsManager'])
				self.popAddProjectW = [QLabel("Name", self.popAddProject),
										QLineEdit(self.popAddProject),
										QPushButton("Add Project",self.popAddProject)]
				self.popAddProjectV =  QVBoxLayout(self.popAddProject)
				self.popAddProjectW[2].clicked.connect(self.AddProject)
				for a in self.popAddProjectW:
					self.popAddProjectV.addWidget(a)
		self.popAddProject.show()





	def DelProject(self):
		db = self._z['Database']
		explorer = self._z['Explorer']
		user = self._z['User']
		if user.getAuthorization("AddProject") == True:
			project = self.popDelProjectW[1].currentText()
			db.dropCollection(project)
			explorer.refreshCbox("Projects")
			explorer.focusCbox("Projects",project)
			self.movePath("Projects", project)
			del project
		self.popDelProjectW[1].clear()
		self.popDelProject.hide()
		del db
		del explorer
		del user





	def popupDelProject(self):
		if self.popDelProject == None:
			self.popDelProject = QDialog(self._z['ProjectsManager'])
			self.popDelProjectW = [ QLabel("Name", self.popDelProject),
								    QComboBox(self.popDelProject),
								    QPushButton("Del Project",self.popDelProject) ]
			self.popDelProjectV = QVBoxLayout(self.popDelProject)
			self.popDelProjectW[2].clicked.connect(self.DelProject)
		for a in self.popDelProjectW:
			self.popDelProjectV.addWidget(a)
		self.popDelProjectW[1].clear()
		for pname in self._z['Database']._projects.keys():
			if pname == "000_None":
				pass
			else:
				self.popDelProjectW[1].addItem(pname)
		self.popDelProject.show()





	def popupAddCategory(self):
		#print(" . Tools.popupAddCategory()")
		if self.popAddCategory == None:
			self.popAddCategory = QDialog(self._z['ProjectsManager'])
			self.popAddCategoryW = [QLabel("Pick a category", self.popAddCategory),
									QComboBox(self.popAddCategory),
									QLabel("Pick a parent or not", self.popAddCategory),
									QComboBox(self.popAddCategory),
									QPushButton("Add Category", self.popAddCategory)]
			self.popAddCategoryV = QVBoxLayout(self.popAddCategory)
			for w in self.popAddCategoryW:
				self.popAddCategoryV.addWidget(w)
			self.popAddCategoryW[4].clicked.connect(self.AddCategory)
		self.popAddCategoryW[1].clear()
		self.popAddCategoryW[3].clear()
		self.popAddCategoryW[3].addItem("None")
		refuse = ["Projects","Dailies","Delivery"]
		for model in self._z['Database'].getAdminModel('Category'):
			if model['Name'] not in refuse :
				self.popAddCategoryW[1].addItem(model['Name'])
				self.popAddCategoryW[3].addItem(model['Name'])
		del refuse
			#print(f". : addItem( {model['Name']} )")
		self.popAddCategory.show()





	def AddCategory(self):
		db = self._z['Database']
		project = self._z['Explorer'].getSelectedProject()
		request = {}
		request['Type'] = "Category"
		request['Name'] = self.popAddCategoryW[1].currentText()
		request['Parent'] = self.popAddCategoryW[3].currentText()
		db.insertInCollection(project,request)
		self._z['Explorer'].Update()
		self.createPath("Category", request['Name'])
		self.popAddCategory.hide()
		del db
		del project
		del request





	def popupDelCategory(self):
		if self.popDelCategory == None:
			self.popDelCategory = QDialog(self._z['ProjectsManager'])
			self.popDelCategoryW = [QLabel("Pick a category", self.popDelCategory),
									QComboBox(self.popDelCategory),
									QPushButton("Del Category")]
			self.popDelCategoryV = QVBoxLayout(self.popDelCategory)
			for w in self.popDelCategoryW:
				self.popDelCategoryV.addWidget(w)
			self.popDelCategoryW[2].clicked.connect(self.DelCategory)
		self.popDelCategoryW[1].clear()
		project = self._z['Explorer'].getSelectedProject()
		for category in self._z['Database'].findInCollection(project,{"Type" : "Category"}):
			self.popDelCategoryW[1].addItem(category['Name'])
			#print(f" . : addItem ( {category['Name']} )")
		self.popDelCategory.show()
		del project





	def DelCategory(self):
		db = self._z['Database']
		project = self._z['Explorer'].getSelectedProject()
		request = {}
		request['Type'] = "Category"
		request['Name'] = self.popDelCategoryW[1].currentText()
		db.deleteInCollection(project, request)
		self._z['Explorer'].Update()
		self.popDelCategory.hide()
		del db
		del project
		del request





	def popupAddRessource(self):
		if self.popAddRessource == None:
			self.popAddRessource = QDialog(self._z['ProjectsManager'])
			self.popAddRessourceW = [QLabel("Ressource Name :", self.popAddRessource),
									 QLineEdit(self.popAddRessource),
									 QPushButton("Add Ressource",self.popAddRessource)]
			self.popAddRessourceV = QVBoxLayout(self.popAddRessource)
			for w in self.popAddRessourceW:
				self.popAddRessourceV.addWidget(w)
			self.popAddRessourceW[2].clicked.connect(self.AddRessource)
		self.popAddRessource.show()





	def AddRessource(self):
		db = self._z['Database']
		project = self._z['Explorer'].getSelectedProject()
		category = self._z['Explorer'].getSelectedCategory()
		request = {}
		request['Type'] = "Ressource"
		request['Category'] = category
		request['Name'] = self.popAddRessourceW[1].text()
		request['Tasks'] = []
		for taskname in self._z['Database'].getCategoryTask():
			request['Tasks'].append({"Task" : taskname , "Status" : "None", "User" : "None"})
		request2 = request.copy()
		if db.existInCollection(project,request2) == False:
			db.insertInCollection(project, request)
			self._z['Explorer'].Update()
			self._z['Explorer'].focusCbox(category,request['Name'])
			self.createPath("Ressource", request['Name'])
			self._z['Grid'].Update()
			self._z['Grid'].Update()
		self.popAddRessourceW[1].clear()
		self.popAddRessource.hide()
		del db
		del project
		del category
		del request





	def popupDelRessource(self):
		if self.popDelRessource == None:
			self.popDelRessource = QDialog(self._z['ProjectsManager'])
			self.popDelRessourceW = [QLabel("Select a ressource", self.popDelRessource),
									 QComboBox(self.popDelRessource),
									 QPushButton("Del Ressource", self.popDelRessource)]
			self.popDelRessourceV = QVBoxLayout(self.popDelRessource)
			for w in self.popDelRessourceW:
				self.popDelRessourceV.addWidget(w)
			self.popDelRessourceW[2].clicked.connect(self.DelRessource)
		for name,ressource in self._z['Database'].getRessources().items():
			if ressource['Category'] == self._z['Explorer'].getSelectedCategory():
				self.popDelRessourceW[1].addItem(name)
		self.popDelRessource.show()





	def DelRessource(self):
		db = self._z['Database']
		project = self._z['Explorer'].getSelectedProject()
		category = self._z['Explorer'].getSelectedCategory()
		request = {}
		request['Type'] = "Ressource"
		request['Category'] = category
		request['Name'] = self.popDelRessourceW[1].currentText()
		db.deleteInCollection(project,request)
		self._z['Explorer'].Update()
		self.popDelRessourceW[1].clear()
		self.popDelRessource.hide()
		self._z['Grid'].Update()
		self._z['Grid'].Update()





	def popupAddTask(self):
		print()





	def AddTask(self):
		print()





	def popupDelTask(self):
		print()





	def DelTask(self):
		print()





	def createPath(self,sender, name):
		if sender == "Projects":
			os.makedirs("./data/" + name, exist_ok=True)
		elif sender == "Category":
			text = self._z['Explorer'].getSelectedProject() + '/'
			text += name + '/'
			os.makedirs("./data/" + text,exist_ok=True)
			del text
		elif sender == "Ressource":
			text = self._z['Explorer'].getSelectedProject() + '/'
			text += self._z['Explorer'].getSelectedCategory() + '/'
			text += name + '/'
			path = "./data/" + text
			os.makedirs(path, exist_ok=True)
			shutil.copyfile("./data/system/nopreview.png", path + "nopreview.png")
			del text
			del path
	def movePath(self, sender, name):
		dd = strftime("%Y-%m-%d", gmtime())
		dh = strftime("%H-%M-%S", gmtime())
		if sender == "Projects":
			shutil.move("./data/" + name, "./data/deleted/" + dd + '/' + dh + '/' + name)
		elif sender == "Category":
			text = self._z['Explorer'].getSelectedProject() + '/'
			text += name + '/'
			shutil.move("./data/" + text, "./data/deleted/" + dd + '/' + dh + '/' + text )
			del text
		elif sender == "Ressource":
			text = self._z['Explorer'].getSelectedProject() + '/'
			text += self._z['Explorer'].getSelectedCategory() + '/'
			text += name + '/'
			shutil.move("./data/" + text, "./data/deleted/" + dd + '/' + dh + '/' + text )
			del text





	def initZone(self,parent):
		self._Zone			= QWidget(parent)
		self._Zone.setStyleSheet("QWidget {background-color : #E4E5DC;}")
		self._mainwindow 	= None
		self.MWResized.connect(self.resize)
		self.initFilter()
		self.initScaleUI()
		self.initPath()
		self.initButtons()
		self.initPopups()





	def resize(self):
		W = self._mainwindow['Width']
		H = self._mainwindow['Height']
		self._Zone.setGeometry(W * 0.16, H * 0.01, W * 0.68, H * 0.15)
		self._Filter.setGeometry(0,0,W * 0.4, H * 0.04)
		self._ScaleSlide.setGeometry(W * 0.415, H * 0.01, W * 0.2, H * 0.02)
		self._ScaleNum.setGeometry(W * 0.63, 0 , W * 0.05, H * 0.04)
		self._Path.setGeometry(0, H * 0.055, W * 0.4, H * 0.04)
		self._OpenPath.setGeometry(W * 0.415, H * 0.055, W * 0.265, H * 0.04)
		self._B1.setGeometry(0,H * 0.11,W * 0.15875, H * 0.04)
		self._B2.setGeometry(W * 0.17375,H * 0.11,W * 0.15875, H * 0.04)
		self._B3.setGeometry(W * 0.3475,H * 0.11,W * 0.15875, H * 0.04)
		self._B4.setGeometry(W * 0.52125,H * 0.11,W * 0.15875, H * 0.04)
		del W
		del H