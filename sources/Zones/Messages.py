from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class Messages(QWidget):
	MWResized = pyqtSignal()
	def __init__(self, parent):
		super(QWidget,self).__init__(parent)
		print("Messages.__init__()")
		self._Zone			= QWidget(parent)
		self._Zone.setStyleSheet("QWidget {background-color : #E4E5DC;}")
		self._Messages 		= QLineEdit(self._Zone)
		mainwindow 			= None
		self.MWResized.connect(self.resize)

	def clear(self):
		self._Messages.clear()

	def setText(self, text):
		self._Messages.clear()
		self._Messages.setText(text)
		del text

	def resize(self):
		print("Messages.resize()")
		W = self._mainwindow['Width']
		H = self._mainwindow['Height']
		self._Zone.setGeometry(W * 0.005, H * 0.93, W * 0.15, H * 0.06)
		self._Messages.setGeometry(0,0, W * 0.15, H * 0.06)