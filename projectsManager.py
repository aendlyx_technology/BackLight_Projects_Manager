import sys
import os
import pathlib
import shutil
from shutil import copyfile
from datetime import datetime
from pymongo import MongoClient
from PyQt5.QtCore import *
from PyQt5 import QtGui
from PyQt5.QtCore import QDir, Qt, QUrl
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from sources.Zones.User import User
from sources.Zones.Explorer import Explorer
from sources.Zones.Tools import Tools
from sources.Zones.Messages import Messages
from sources.Zones.Grid import Grid
from sources.Zones.Progress import Progress
from sources.Zones.Preview import Preview
from sources.Zones.Comments import Comments
from sources.Zones.Modes import Modes
from sources.Database.Database import Database

class ProjectsManager(QMainWindow):
    def __init__(self):
        super(QMainWindow,self).__init__()
        #print("ProjectsManager.__init__()")

        # MAIN
        #print("ProjectsManager.MAIN")
        self._Painter = self.Painter()
        self._Window = self.Window()

        # DATABASE
        self._Database = Database(self)

        # SUBCLASSES
        #print("ProjectsManager.SUBCLASSES")
        self._User = User(self)
        self._Tools = Tools(self)
        self._Explorer = Explorer(self)
        # self._Messages = Messages(self)
        self._Grid = Grid(self)
        # self._Progress = Progress(self)
        # self._Preview = Preview(self)
        # self._Comments = Comments(self)
        # self._Modes = Modes(self)
        self.z(self)
        
        self._Explorer.Update()

    def z(self,parent):
        self._SubClasses = []
        self._SubClasses.append(self._Database)
        self._SubClasses.append(self._User)
        self._SubClasses.append(self._Tools)
        self._SubClasses.append(self._Explorer)
        # self._SubClasses.append(self._Messages)
        self._SubClasses.append(self._Grid)
        # self._SubClasses.append(self._Progress)
        # self._SubClasses.append(self._Preview)
        # self._SubClasses.append(self._Comments)
        # self._SubClasses.append(self._Modes)
        for sub in self._SubClasses:
            sub._z = {}
            sub._z['ProjectsManager'] = parent
            sub._z['Database'] = parent._Database
            sub._z['User'] = parent._User
            sub._z['Tools'] = parent._Tools
            sub._z['Explorer'] = parent._Explorer
            # sub._z['Messages'] = parent._Messages
            sub._z['Grid'] = parent._Grid
            # sub._z['Progress'] = parent._Progress
            # sub._z['Preview'] = parent._Preview
            # sub._z['Comments'] = parent._Comments
            # sub._z['Modes'] = parent._Modes

    def Window(self):
        self.setGeometry(400,400,1920,1080)
        Window = {}
        Window['Width']  = self.width()
        Window['Height'] = self.height()
        return Window

    def updateMWGSub(self):
        #print("updateMWGSub")
        self._User._mainwindow = self._Window
        self._Explorer._mainwindow = self._Window
        self._Tools._mainwindow = self._Window
        # self._Messages._mainwindow = self._Window
        self._Grid._mainwindow = self._Window
        # self._Progress._mainwindow = self._Window
        # self._Preview._mainwindow = self._Window
        # self._Comments._mainwindow = self._Window
        # self._Modes._mainwindow = self._Window


    def resizeEvent(self, event):
        #print('resizeEvent')
        self._Window['Width']  = self.width()
        self._Window['Height'] = self.height()
        self.updateMWGSub()
        self._User.MWResized.emit()
        self._Explorer.MWResized.emit()
        self._Tools.MWResized.emit()
        # self._Messages.MWResized.emit()
        self._Grid.MWResized.emit()
        # self._Progress.MWResized.emit()
        # self._Preview.MWResized.emit()
        # self._Comments.MWResized.emit()
        # self._Modes.MWResized.emit()
        
    def Painter(self):
        self.colorBackground = QColor(63,64,61)
        self.painterBackground = QPainter()
        
    def paintEvent(self, event):
        self.painterBackground.begin(self)
        self.painterBackground.setPen(self.colorBackground)
        self.painterBackground.setBrush(self.colorBackground)
        self.painterBackground.drawRect(QRect(0,0,self._Window['Width'], self._Window['Height']))
        self.painterBackground.end()

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Shift:
            print('Shift Pressed')
            if self._Grid._shift == True:
                print()
                # self._Grid._shift = False
                # self._Grid.Update()
            else:
                self._Grid._shift = True



if __name__ == '__main__':
    app = QApplication(sys.argv)
    pm = ProjectsManager()
    pm.show()
    sys.exit(app.exec_())